const express = require('express')
const app = express()
const port = 3000
app.use(express.json())

let users = [{ name: "user1", color: "red" }, { name: "user2", color: "blue" }, { name: "user3", color: "green" }];

app.get('/users/all', (req, res) => {
    res.send(users);
})
app.put('/users/add', (req, res) => {
    const { name, color } = req.body;
    if (name == '' || color == '')
        res.status(400).send("Неправильные данные");
    else {
        if (users.find(((obj) => { return name == obj.name })) != undefined)
            res.status(400).send("Пользователь существует");
        else {
            users.push({ name: name, color: color });
            res.status(200).send(users);
        }
    }
})
app.delete('/users/delete', (req, res) => {
    const { name } = req.body;
    if (name == '')
        res.status(400).send("Неправильные данные");
    else {
        const index = users.findIndex(((obj) => { return name == obj.name }));
        if (index != -1) {
            const f1 = index == 0 ? [] : users.slice(0, index);
            const f2 = index == users.length - 1 ? [] : users.slice(index + 1, users.length);
            users = f1.concat(f2);
            res.status(200).send(users);
        }
        else {
            res.status(400).send("Пользователь не существует");
        }
    }
})
app.listen(port, () => {
    console.log(`Example app listening on port http://localhost:${port}`)
})

